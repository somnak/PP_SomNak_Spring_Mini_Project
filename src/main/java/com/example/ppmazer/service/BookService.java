package com.example.ppmazer.service;

import com.example.ppmazer.model.Book;

import java.util.List;
public interface BookService {
    //List<Book> getBooks(int limit,int offset);
    List<Book> getBooks();
    List<Book> getLatestBooks();
    boolean updateBook(Book book);
    boolean deleteBook(int id);
    boolean addBook(Book book);
    Book searchBook(int id);
    Integer getCount();
}
