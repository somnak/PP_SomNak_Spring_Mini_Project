package com.example.ppmazer.service.BookServiceImp;

import com.example.ppmazer.model.Category;
import com.example.ppmazer.repository.CategoryRepository;
import com.example.ppmazer.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll() {
        return this.categoryRepository.getAll();
    }

    @Override
    public Integer count() {
        return this.categoryRepository.count();
    }
}
