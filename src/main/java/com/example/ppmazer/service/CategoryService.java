package com.example.ppmazer.service;

import com.example.ppmazer.model.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();
    Integer count();
}
