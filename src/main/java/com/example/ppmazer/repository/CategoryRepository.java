package com.example.ppmazer.repository;

import com.example.ppmazer.model.Category;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Select("select * from tb_category order by id")
    List<Category> getAll();


    @Select("select count(*) from tb_category")
    Integer count();

}
