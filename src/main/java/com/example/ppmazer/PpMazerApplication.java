package com.example.ppmazer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PpMazerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PpMazerApplication.class, args);
	}
}
