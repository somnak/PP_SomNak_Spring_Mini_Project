package com.example.ppmazer.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.example.ppmazer.repository")
public class MyBatisConfiguration {
    private DataSource dts;
    public MyBatisConfiguration(@Qualifier("dataSource") DataSource dts) {
        this.dts=dts;
    }
    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager(){
        return new DataSourceTransactionManager(dts);
    }
}